#include <cblas.h>
#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>


PyObject* axpy(PyObject *self, PyObject *args)
{
  Py_complex alpha;
  PyArrayObject* x;
  PyArrayObject* y;
  if (!PyArg_ParseTuple(args, "DOO", &alpha, &x, &y))
    return NULL;

  int n = PyArray_DIMS(x)[0];
  int one = 1;

  if (PyArray_DESCR(x)->type_num == NPY_DOUBLE)
    cblas_daxpy(n, alpha.real, PyArray_DATA(x), one, PyArray_DATA(y), one);
  else
    cblas_zaxpy(n, (void*)&alpha, PyArray_DATA(x), one, PyArray_DATA(y), one);

  Py_RETURN_NONE;
}


static PyMethodDef methoddef[] = {
  {"axpy", axpy, METH_VARARGS, "my axpy"},
  {NULL, NULL, 0, NULL}
};


static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  "_mylib",
  "My C-extension",
  -1,
  methoddef
};


PyMODINIT_FUNC PyInit__mylib(void)
{
  return PyModule_Create(&moduledef);
}
