from setuptools import setup, Extension

setup(name='_mylib', version='1.0',
      ext_modules=[Extension('_mylib', ['src/_mylib.c'], libraries=['blas'])])
