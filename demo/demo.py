import numpy as np
import _mylib

n = 5

alpha = 2.0
x = 2 * np.ones(n)
y = np.ones(n)

print('1', alpha * x + y)

_mylib.axpy(alpha, x, y)
print('2', y)


alpha = 2.0 - 3.0j
x = 2 * np.ones(n) - 1j * np.ones(n)
y = np.ones(n) - 2j * np.ones(n)

print('1', alpha * x + y)

_mylib.axpy(alpha, x, y)
print('2', y)
