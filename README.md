# Python C Extension Demo

Adapted from C extensions in [GPAW](https://gitlab.com/gpaw/gpaw).


## Usage

1. Build extension

       python setup.py build_ext

2. Add extension to PYTHONPATH

       export PYTHONPATH=$(echo build/lib.*):$PYTHONPATH

3. Run demo

       python demo/demo.py


## Alternative

* Build extension manually

       gcc -std=c99 -O2 -fPIC -I$(echo /usr/include/python*) src/_mylib.c -shared -lblas -o _mylib.so
